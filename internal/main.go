package main

import "C"
import (
	"fmt"
	"io/ioutil"
	"time"

	"gitee.com/yrwy/msgo/pkg/archive/tar"
	"gitee.com/yrwy/msgo/pkg/archive/zip"
	"gitee.com/yrwy/msgo/pkg/datetime"
	"gitee.com/yrwy/msgo/pkg/math"
	"gitee.com/yrwy/msgo/pkg/net"
)

func main() {
	a := math.MakeFractional(-3, 39)
	fmt.Println(a, a.Repeating(), a.Value())

	datetime_example()
}

func compress_example() {
	zip.Compress("../a.zip", ".")
	zip.Decompress("../a.zip", "d:/a")
	tar.Compress("../b.tar.gz", ".")
	tar.Compress("../c.tar", ".")
	err := tar.Decompress("../b.tar.gz", "d:/b")
	if err != nil {
		println("tar.Decompress:", err.Error())
	}
	err = tar.Decompress("../c.tar", "d:/c")
	if err != nil {
		println("tar.Decompress:", err.Error())
	}
}

func net_example() {
	zip.Compress("../a.zip", ".")
	rep, err := net.Post("http://127.0.0.1:4101/route", nil, nil, map[string]string{"file": "../a.zip"}, time.Second*time.Duration(3))
	if err != nil {
		fmt.Println(err)
	}
	defer rep.Body.Close()
	fmt.Println(rep)
	fmt.Println(rep.Header)
	body, _ := ioutil.ReadAll(rep.Body)
	fmt.Println(string(body))
}

func datetime_example() {
	d := datetime.NowDate()
	t := datetime.NowTime()
	dt := datetime.NowDatetime()
	fmt.Println(d, t, dt)

	tt := dt.ToSysTime()
	fmt.Println(tt)

	s := "2020-12-21 01:20:09"
	tt, err := datetime.String2SysTime(s)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(tt)
	}
	d = datetime.SysTime2Date(tt)
	d = d.AddDays(-10)
	dt = datetime.SysTime2Datetime(tt)
	dt = dt.AddHours(3)
	fmt.Println(d, dt)
}
