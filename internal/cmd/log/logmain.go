package main

import (
	"fmt"
	"os"
	"runtime"
	"time"

	. "gitee.com/yrwy/msgo/pkg/io/log"
)

func tryWriteLogMsg(idx int, logger Logger) {
	i := 1
	Debug("这是一条Debug信息")
	Debugln(idx, "这是一条Debug信息", i)
	i++
	Debugf("%d 这是一条Debug信息 %d", idx, i)
	Info("这是一条Info信息")
	Infoln(idx, "这是一条Info信息", i)
	i++
	Infof("%d 这是一条Info信息 %d", idx, i)
	Error("这是一条Error信息")
	Errorln(idx, "这是一条Error信息", i)
	i++
	Errorf("%d 这是一条Error信息 %d", idx, i)
	Fatal("这是一条Fatal信息")
	Fatalln(idx, "这是一条Fatal信息", i)
	i++
	Fatalf("%d 这是一条Fatal信息 %d", idx, i)
	i++
	Logln(DEBUG, idx, i, "all")
	i++
	Logln(INFO, idx, i, "all")
	i++
	Logln(WARN, idx, i, "all")
	i++
	Logln(ERROR, idx, i, "all")
	i++
	logger.Logln(DEBUG, idx, i, "logger")
	i++
	logger.Logln(INFO, idx, i, "logger")
	i++
	logger.Logln(WARN, idx, i, "logger")
	i++
	logger.Logln(ERROR, idx, i, "logger")
}

func main() {
	pc, file, line, ok := runtime.Caller(0)
	if ok {
		fc := runtime.FuncForPC(pc)
		name := "???"
		if fc != nil {
			name = fc.Name()
		}
		fmt.Println(file, name, line)
	}
	fp, _ := os.OpenFile("err.log", os.O_CREATE|os.O_WRONLY, os.ModePerm)
	errHandler := func(w LogWriter, err error, log string) {
		fp.WriteString(fmt.Sprintln(err))
	}
	cw := NewConsoleWriter(LWStrLevel("ERROR"))
	dur := time.Hour * 2
	fr, err := NewFiler(FileMaxSize(10*1024*1024), FileSaveDays(2), FileDelete(1, 37, 0), FileDuration(dur))
	if err != nil {
		fmt.Println("NewFiler error:", err)
		return
	}
	f := NewTextFormatter(FProperties(LstdFlags), FLineFeed(), FMsgprefix("msg"))
	fw := NewLogWriter(LWStrLevel("Info"), LWithWriter(fr), LWErrHandler(errHandler))
	w := NewMultiWriter(cw, fw)
	logger := NewLogger(LogSetWriter(w), LogFormatter(f))
	SetLogger(logger)
	for i := 0; i < 9; i++ {
		go func(i int) {
			for {
				tryWriteLogMsg(i, logger)
				time.Sleep(time.Second * time.Duration(i))
			}
		}(i + 1)
	}
	for {
		tryWriteLogMsg(10, logger)
		time.Sleep(time.Millisecond * 500)
	}
}
