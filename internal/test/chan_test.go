package test

/** test chan
测试结果：
1.通道发送满后不会被select
2.未初始化的通道不会被select，未初始化的通道接收会一直阻塞无法退出，造成泄漏
3.通道关闭后通道会被select，通道关闭后接收数据会panic,但能被recover恢复
*/

import (
	"fmt"
	"testing"
)

func TestChanRecv(t *testing.T) {
	c := make(chan int, 10)
cc:
	for i := 0; i < 100; i++ {
		select {
		case c <- i:
		default:
			fmt.Println("break")
			close(c)
			break cc
		}
	}
	for i := range c {
		fmt.Println(i)
	}
	fmt.Println("over")
}

//通道发送满后不会被select

func TestChanNil(t *testing.T) {
	var c chan int
	for i := 0; i < 100; i++ {
		select {
		case c <- i:
		default:
			fmt.Println("break")
			break
		}
	}
	for i := range c { //卡住
		fmt.Println(i)
	}
	fmt.Println("over")
}

func TestChanNil2(t *testing.T) {
	var c chan int
cc:
	for i := 0; i < 100; i++ {
		select {
		case c <- i:
		default:
			fmt.Println("break")
			break cc
		}
	}
	for { //卡住
		if i, ok := <-c; ok {
			fmt.Println(i)
		} else {
			break
		}
	}
	fmt.Println("over")
}

//未初始化的通道不会被select
//未初始化的通道接收会一直阻塞无法退出，造成泄漏

func TestChanClose(t *testing.T) {
	defer func() {
		if p := recover(); p != nil {
			fmt.Println(p)
		}
	}()
	c := make(chan int)
	closed := false
cc:
	for i := 0; i < 100; i++ {
		select {
		case c <- i: //close后panic
		default:
			if closed {
				fmt.Println("break")
				break cc
			}
			close(c)
		}
	}
}

// 通道关闭后通道会被select
// 通道关闭后接收数据会panic,但能被recover恢复
