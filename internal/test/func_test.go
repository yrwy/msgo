package test

import (
	"fmt"
	"testing"
)

//go test -bench=Byte -benchtime=10s
func BenchmarkByte(b *testing.B) {
	s := "测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串\n"
	a := []byte(s)
	for i := 0; i < b.N; i++ {
		fmt.Printf("%s", a)
	}
}

//go test -bench=String -benchtime=10s
func BenchmarkString(b *testing.B) {
	s := "测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串测试字符串\n"
	a := []byte(s)
	for i := 0; i < b.N; i++ {
		fmt.Print(string(a))
	}
}