package graph

// Directed graph
type DiGraph[V comparable, E any, W any] struct {
	order     map[V]map[V]E
	preorder  map[V]map[V]E
	Weight    func(E) W
	AddWeight func(W, W) W
}
