package graph

type Route[W any] struct {
	w    W
	once bool //唯一路径
}

type Edge[E any, W any] struct {
	e E
	r Route[W]
}
