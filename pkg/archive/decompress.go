package archive

import (
	"path"
	"strings"

	mstar "gitee.com/yrwy/msgo/pkg/archive/tar"
	mszip "gitee.com/yrwy/msgo/pkg/archive/zip"
	"gitee.com/yrwy/msgo/pkg/errors"
)

func Compress(dest string, src ...string) error {
	ext := path.Ext(dest)
	if strings.EqualFold(ext, ".zip") {
		return mszip.Compress(dest, src...)
	}
	if strings.EqualFold(ext, ".tar") || (len(dest) > 7 && strings.EqualFold(dest[len(dest)-7:], ".tar.gz")) {
		return mstar.Compress(dest, src...)
	}
	return errors.ErrNotSupport
}

func Decompress(src string, destDir string) ([]string, error) {
	ext := path.Ext(src)
	if strings.EqualFold(ext, ".zip") {
		return mszip.Decompress(src, destDir)
	}
	if strings.EqualFold(ext, ".tar") || (len(src) > 7 && strings.EqualFold(src[len(src)-7:], ".tar.gz")) {
		return mstar.Decompress(src, destDir)
	}
	return nil, errors.ErrNotSupport
}
