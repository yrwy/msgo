package archive

import (
	"fmt"
	"testing"
)

func TestCompress(t *testing.T) {
	err := Compress("d:/temp/a.zip", "d:/temp/matrix", "d:/temp/storagegx")
	if err != nil {
		fmt.Println(err)
	}
	err = Compress("d:/temp/a.tar.gz", "d:/temp/matrix", "d:/temp/storagegx")
	if err != nil {
		fmt.Println(err)
	}
}
