package math

import (
	"fmt"
	"strconv"
	"testing"
)

func TestFractional(t *testing.T) {
	fmt.Println(strconv.ParseFloat("+Inf", 0))
	fmt.Println(strconv.ParseInt("99999999999999999999999999", 0, 0))
	fmt.Println(Fractional{-1, 0}, Fractional{1, 0})
	f13 := Fractional{1, 13}
	fmt.Println(f13, f13.Repeating(), f13.Value())
	fmt.Println(MakeFractional(4, 8))
	f := 0.07692307692307693
	fmt.Println(Float2Fractional(f))
	fmt.Println(Float2Fractional(0.625))
	fmt.Println(Str2Fractional("-3/13"))
	fmt.Println("over")
}
