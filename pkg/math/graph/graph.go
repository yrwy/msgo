package graph

type Grapher[T comparable] interface {
	Adjacency(prev T) []T
	NewTree() Tree[T]
	Weight(prev T, next T) int
}

type GraphMap[T comparable] map[T][]T

func (x GraphMap[T]) Adjacency(prev T) []T {
	return x[prev]
}

func (x GraphMap[T]) NewTree() Tree[T] {
	return make(GraphMapTree[T])
}

// breadth-first search
func (x GraphMap[T]) BFS(s T) GraphMapTree[T] {
	return BFS[T](x, s).(GraphMapTree[T])
}

// Depth-first search
func (x GraphMap[T]) DFS(s T) GraphMapTree[T] {
	return DFS[T](x, s).(GraphMapTree[T])
}

func (x GraphMap[T]) BreadthFirstSearch(s T, max_degree int, degree DegreeFunc[T]) GraphMapTree[T] {
	return BreadthFirstSearch[T](x, s, max_degree, degree).(GraphMapTree[T])
}

func (x GraphMap[T]) DepthFirstSearch(s T, max_degree int, degree DegreeFunc[T]) GraphMapTree[T] {
	return DepthFirstSearch[T](x, s, max_degree, degree).(GraphMapTree[T])
}
