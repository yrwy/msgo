package graph

type Weight interface {
	Add(Weight) Weight
	Less(Weight) bool
}
