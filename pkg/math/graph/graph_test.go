package graph

import (
	"testing"
)

func TestSearch(t *testing.T) {
	m := make(GraphMap[int])
	m[1] = append(m[1], 2)
	m[2] = append(m[2], 3, 4, 6, 7)
	m[3] = append(m[3], 8)
	m[4] = append(m[4], 5)
	m[6] = append(m[6], 9)
	m[5] = append(m[5], 10)
	tree := BFS[int](m, 1)
	for i, v := range tree.(GraphMapTree[int]) {
		println(i, v)
	}
	println()
	tree = DFS[int](m, 1)
	for i, v := range tree.(GraphMapTree[int]) {
		println(i, v)
	}
}
