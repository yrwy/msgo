package math

import (
	"fmt"
	"testing"
)

// go test -run=TestStringSimilarityRune
func TestStringSimilarityRune(t *testing.T) {
	s1 := "塘田市入"
	s2 := []string{"湖南塘田市站", "终止计费位置桩号"}
	for _, s := range s2 {
		fmt.Println(s1, s, StringSimilarityRune(s1, s))
	}

}

// go test -run=TestMatchWildcard
func TestMatchWildcard(t *testing.T) {
	p := "abc*abc*ab?ab******"
	s := "abcavabckkabcabca"
	if !MatchWildcard(p, s) {
		t.Fatal(p, s, "not match")
	}
}
