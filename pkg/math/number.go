package math

import (
	"golang.org/x/exp/constraints"
)

// 计算二进制中1的个数
func BinaryCount[N constraints.Unsigned](n N) int {
	count := 0
	for n != 0 {
		n = n & (n - 1) //每次都能消除最后面的一个1
		count++
	}
	return count
}

func Less[N constraints.Ordered](a, b N) bool {
	return a < b
}
