package math

import (
	"crypto"
	_ "crypto/md5"
	_ "crypto/sha1"
	_ "crypto/sha256"
	_ "crypto/sha512"
	"encoding/hex"
)

func Hash(h crypto.Hash, content []byte) string {
	if h.Available() {
		ha := h.New()
		v := ha.Sum(content)            // 计算哈希值
		return hex.EncodeToString(v[:]) // 将哈希值转换为字符串
	}
	return ""
}

func MD5(content []byte) string {
	return Hash(crypto.MD5, content)
}

func SHA1(content []byte) string {
	return Hash(crypto.SHA1, content)
}

func SHA224(content []byte) string {
	return Hash(crypto.SHA224, content)
}

func SHA256(content []byte) string {
	return Hash(crypto.SHA256, content)
}

func SHA384(content []byte) string {
	return Hash(crypto.SHA384, content)
}

func SHA512(content []byte) string {
	return Hash(crypto.SHA512, content)
}
