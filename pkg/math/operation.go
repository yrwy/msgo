//go:build go1.21

package math

import (
	"cmp"

	"golang.org/x/exp/constraints"
)

// can + +=
type Addable interface {
	cmp.Ordered | constraints.Complex
}

func Add[N Addable](a N, nums ...N) N {
	for _, num := range nums {
		a += num
	}
	return a
}

type Multiplicative interface {
	constraints.Integer | constraints.Float | constraints.Complex
}

// can * *=
func Multiply[N Multiplicative](a N, nums ...N) N {
	for _, num := range nums {
		a *= num
		if a == 0 {
			break
		}
	}
	return a
}
