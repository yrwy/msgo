package kv

import (
	"sync"
	"time"
)

type MapCache[K comparable] struct {
	rwm       sync.RWMutex
	mc        map[K]any
	nextPoint time.Time
}
