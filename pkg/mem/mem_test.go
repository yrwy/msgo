package mem

import (
	"fmt"
	"reflect"
	"testing"
)

func TestConvertSlice(t *testing.T) {
	s := []any{1, 2, 3, 4, 5, 6}
	v_ := reflect.ValueOf(s)
	switch v_.Kind() {
	case reflect.Array, reflect.Slice, reflect.String:
		n := v_.Len()
		r := make([]int, n)
		for i := 0; i < n; i++ {
			val := v_.Index(i)
			reflect.ValueOf(&r[i]).Elem().Set(val.Elem())
		}
		fmt.Println(r)
	}
	if r, ok := ConvertSlice[int8](s); ok {
		fmt.Println(r)
	} else {
		fmt.Println("false")
		t.Fail()
	}
	type Int int64
	if r, ok := ConvertSlice[Int](s); ok {
		fmt.Println(r)
	} else {
		fmt.Println("false")
		t.Fail()
	}
}
