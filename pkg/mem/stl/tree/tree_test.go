package tree

import (
	"fmt"
	"testing"
)

func frontTraversal(tree Rbtree[int, int], minI, maxI int) bool {
	i := minI
	b := tree.Front()
	for b != nil {
		if tree.Key(b.Value) != i {
			fmt.Println("Front traversal :", b.Value, i)
			return false
		}
		i++
		b = b.Next()
	}
	if i-1 != maxI {
		fmt.Println("Front traversal end in:", i)
		return false
	}
	return true
}

func backTraversal(tree Rbtree[int, int], minI, maxI int) bool {
	i := maxI
	b := tree.Back()
	for b != nil {
		if tree.Key(b.Value) != i {
			fmt.Println("Back traversal :", b.Value, i)
			return false
		}
		i--
		b = b.Prev()
	}
	if i != minI-1 {
		fmt.Println("Back traversal end in:", i)
		return false
	}
	return true
}

//find [min, max]
func boundTraversal(tree Rbtree[int, int], minI, maxI int) bool {
	i := minI
	b := tree.LowerBound(minI)
	e := tree.UpperBound(maxI)
	for b != e && b != nil {
		if tree.Key(b.Value) != i {
			fmt.Println("Bound traversal :", b.Value, i)
			return false
		}
		i++
		b = b.Next()
	}
	if i != maxI+1 {
		fmt.Println("Bound traversal end in:", i)
		return false
	}
	return true
}

func TestRbtree(t *testing.T) {
	tree := NewRbtree[int, int](func(v int) int {
		return v
	}, func(l, r int) bool {
		return l < r
	})
	const maxCount = 123456
	for i := 1; i <= maxCount; i++ {
		tree.Insert(i)
		if tree.Len() != i {
			fmt.Println("rbtree len:", tree.Len(), i)
			t.Fatal("")
		}
	}
	if !frontTraversal(tree, 1, maxCount) {
		t.Fatal("frontTraversal")
	}
	if !backTraversal(tree, 1, maxCount) {
		t.Fatal("backTraversal")
	}
	if !boundTraversal(tree, 10, 1000) {
		t.Fatal("boundTraversal")
	}
	fmt.Println("delete")
	for i := 120001; i <= maxCount; i++ {
		tree.Remove(i)
		if tree.Len() != 120000+maxCount-i {
			fmt.Println("rbtree len:", tree.Len(), i)
			t.Fatal("")
		}
	}
	if !frontTraversal(tree, 1, 120000) {
		t.Fatal("frontTraversal")
	}
	if !backTraversal(tree, 1, 120000) {
		t.Fatal("backTraversal")
	}
	if !boundTraversal(tree, 10, 1000) {
		t.Fatal("boundTraversal")
	}
}
