package stl

type Lesser[T any] interface {
	Less(r T) bool
}

/*
// 小于
func Less[K any, T Lesser[K]](left, right T) bool {
	return left.Less(right)
}

// 大于
func Greater[T Lesser](left, right T) bool {
	return right.Less(Lesser(left))
}

// 相等
func Equal[T Lesser](left, right T) bool {
	return !left.Less(right) && !right.Less(left)
}

// 小于等于
func LessEqual[T Lesser](left, right T) bool {
	return !right.Less(left) //不大于
}

// 大于等于
func GreaterEqual[T Lesser](left, right T) bool {
	return !left.Less(right) //不小于
}
*/
