package heap

import (
	"math/rand"
	"testing"
)

func checkHeap(h *Heap[int]) bool {
	n := h.Len()
	for i := 0; i < n; i++ {
		l := i*2 + 1
		if l < n && h.d.s[l] < h.d.s[i] {
			return false
		}
		r := l + 1
		if r < n && h.d.s[r] < h.d.s[i] {
			return false
		}
	}
	return true
}

func TestHeap(t *testing.T) {
	h := NewHeap[int](func(l, r int) bool {
		return l < r
	})
	for i := 0; i < 100000; i++ {
		h.Push(rand.Intn(100000))
		if !checkHeap(h) {
			t.Fatal()
		}
	}
}
