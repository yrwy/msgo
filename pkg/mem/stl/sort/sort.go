package sort

import (
	"slices"
)

func Slice[T any](data []T, less func(l, r T) bool) {
	slices.SortFunc[[]T, T](data, func(a, b T) int {
		if less(a, b) {
			return -1
		} else if less(b, a) {
			return 1
		}
		return 0
	})
}
