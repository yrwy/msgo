package c

import "C"
import (
	"unsafe"
)

func Str2Pointer(s string) C.uintptr_t {
	return C.uintptr_t((uintptr)(unsafe.Pointer(&[]byte(s)[0])))
}

func Str2PChar(s string) *C.char {
	return (*C.char)(unsafe.Pointer(&[]byte(s)[0]))
}
