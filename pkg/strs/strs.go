package strs

import (
	"strings"
	"unsafe"
)

// 能否表示为二进制
func IsBinary(s string) bool {
	for _, i := range s {
		switch i {
		case '0', '1':
			continue
		default:
			return false
		}
	}
	return true
}

// 能否表示为八进制
func IsOctal(s string) bool {
	for _, i := range s {
		switch i {
		case '0', '1', '2', '3', '4', '5', '6', '7':
			continue
		default:
			return false
		}
	}
	return true
}

// 能否表示为十进制
func IsDecimal(s string) bool {
	for _, i := range s {
		switch i {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			continue
		default:
			return false
		}
	}
	return true
}

// 能否表示为十六进制
func IsHex(s string) bool {
	for _, i := range s {
		switch i {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F':
			continue
		default:
			return false
		}
	}
	return true
}

// 取字符串底层数组地址
func StrDataAddr(s string) *byte {
	return unsafe.StringData(s)
}

func Bytes2Str(bs []byte) string {
	return unsafe.String((*byte)(unsafe.Pointer(&bs)), len(bs))
}

func WithSuffixFold(s, suffix string) bool {
	return len(s) >= len(suffix) && strings.EqualFold(s[len(s)-len(suffix):], suffix)
}

// like strings.HasPrefix
func WithPrefixFold(s, prefix string) bool {
	return len(s) >= len(prefix) && strings.EqualFold(s[:len(prefix)], prefix)
}

func EqualN(s, t string, n int) bool {
	if n <= 0 {
		return true
	}
	if len(s) > n {
		s = s[:n]
	}
	if len(t) > n {
		t = t[:n]
	}
	return s == t
}

func EqualFoldN(s, t string, n int) bool {
	if n <= 0 {
		return true
	}
	if len(s) > n {
		s = s[:n]
	}
	if len(t) > n {
		t = t[:n]
	}
	return strings.EqualFold(s, t)
}

func EnableString(s string) bool {
	switch len(s) {
	case 1:
		return s == "1" || strings.EqualFold(s, "y")
	case 2:
		return strings.EqualFold(s, "on")
	case 3:
		return strings.EqualFold(s, "yes")
	case 4:
		return strings.EqualFold(s, "true")
	}
	return s == "是"
}
