package strs

import (
	"strconv"
)

//万位分割法显示数字
func Utoa_cn(n uint64) string {
	r := strconv.FormatUint(n, 10)
	size := len(r)
	i := size % 4
	ret := r[:i]
	size -= i
	for size >= 4 {
		ret += "'" + r[i:i+4]
		i += 4
		size -= 4
	}
	return ret
}

func Stoi(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}
