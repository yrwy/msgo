package strs

import (
	"fmt"
	"testing"
)

func TestUtoa(t *testing.T) {
	var n uint64 = 123456789
	t.Log(Utoa_cn(n))
	n = 0xffffffffffff
	t.Log(Utoa_cn(n))
}

func TestString(t *testing.T) {
	s := "这是字符串"
	fmt.Printf("%s %p %x\n", s, &s, StrDataAddr(s))
	a := []byte(s)
	fmt.Printf("%s %p %p\n", a, &a, &a[0])
	ss := string(a)
	fmt.Printf("%s %p %x\n", ss, &ss, StrDataAddr(ss))
	sss := Bytes2Str(a)
	fmt.Printf("%s %p %x\n", sss, &sss, StrDataAddr(sss))

}
