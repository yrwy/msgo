package storage

type IData interface {
	GetRow() []any
}

type IStore interface {
	Write(IData) error
	Writes([]IData) (int, error) //返回写入个数或者错误信息
}
