package net

import (
	"gitee.com/yrwy/msgo/pkg/mem/kv"
)

type Value = kv.Map[string]

func GetValue(v Value, key string) (Value, error) {
	return v.GetMap(key)
}
