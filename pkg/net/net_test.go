package net

import (
	"fmt"
	"net"
	"testing"
)

func TestLocalIP(t *testing.T) {
	ips, err := GetLocalIPs()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(ips)
	}
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		fmt.Println(err)
	}
	for _, address := range addrs {
		// 检查IP地址，其他类型的地址(如link-local或者loopback)忽略
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() && ipnet.IP.To4() != nil {
			fmt.Println(ipnet, address.Network(), address.String())
		}
	}
}
