//go:build windows

package os

import (
	"os"

	mio "gitee.com/yrwy/msgo/pkg/io"
)

/*
func CreateDirs(path string) error {
	if mio.IsDir(path) {
		return nil
	}
	i := len(path) - 1
	for i >= 0 && os.IsPathSeparator(path[i]) {
		i--
	}
	for i >= 0 && !os.IsPathSeparator(path[i]) {
		i--
	}
	if i < 0 {
		i = 0
	}

	// If there is a parent directory, and it is not the volume name,
	// recurse to ensure parent directory exists.
	if parent := path[:i]; len(parent) > len(filepath.VolumeName(path)) {
		err := CreateDirs(parent)
		if err != nil {
			return err
		}
	}

	// Parent now exists; invoke Mkdir and use its result.
	dir, err := syscall.UTF16PtrFromString(path)
	if err != nil {
		return err
	}

	sa := syscall.SecurityAttributes{Length: 0}
	sddl := "D:P(A;OICI;GA;;;BA)(A;OICI;GA;;;SY)"
	sd, err := winio.SddlToSecurityDescriptor(sddl)
	if err != nil {
		return err
	}
	sa.Length = uint32(unsafe.Sizeof(sa))
	sa.InheritHandle = 1
	sa.SecurityDescriptor = uintptr(unsafe.Pointer(&sd[0]))

	err = syscall.CreateDirectory(dir, &sa)
	if err != nil {
		// Handle arguments like "foo/." by
		// double-checking that directory doesn't exist.
		dir, err1 := os.Lstat(path)
		if err1 == nil && dir.IsDir() {
			return nil
		}
		return err
	}
	return nil
}
*/

func CreateDirs(dir string) error {
	if mio.IsDir(dir) {
		return nil
	}
	return os.MkdirAll(dir, os.ModePerm)
}
