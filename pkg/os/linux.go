//go:build linux

package os

import (
	"os"

	mio "gitee.com/yrwy/msgo/pkg/io"
)

func CreateDirs(dir string) error {
	if mio.IsDir(dir) {
		return nil
	}
	return os.MkdirAll(dir, os.ModePerm)
}
