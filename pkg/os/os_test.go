package os

import (
	"fmt"
	"testing"
)

func TestCaller(t *testing.T) {
	type C struct {
		CallFrames
	}
	c := C{NewCallers()}
	fmt.Println(c)
	fmt.Printf("%+s\n", c)
	fmt.Printf("%+v\n", c)
}
