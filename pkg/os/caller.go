package os

import (
	"fmt"
	"io"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
)

type Caller uintptr

func NewCallerDepth(skip int) Caller {
	pc, _, _, ok := runtime.Caller(skip + 1)
	if ok {
		return Caller(pc)
	}
	return 0
}

func NewCaller() Caller {
	return NewCallerDepth(2)
}

func (f Caller) pc() uintptr { return uintptr(f) }

// file returns the full path to the file that contains the
func (f Caller) File() string {
	fn := runtime.FuncForPC(f.pc())
	if fn == nil {
		return "unknown"
	}
	file, _ := fn.FileLine(f.pc())
	return file
}

// line returns the line number of source code of the
func (f Caller) Line() int {
	fn := runtime.FuncForPC(f.pc())
	if fn == nil {
		return 0
	}
	_, line := fn.FileLine(f.pc())
	return line
}

// name returns the name of this function, if known.
func (f Caller) Name() string {
	fn := runtime.FuncForPC(f.pc())
	if fn == nil {
		return "unknown"
	}
	return fn.Name()
}

// Format formats the Caller according to the fmt.Formatter interface.
func (f Caller) Format(s fmt.State, verb rune) {
	fn := runtime.FuncForPC(f.pc())
	file, line := fn.FileLine(f.pc())
	switch verb {
	case 's', 'v':
		switch {
		case s.Flag('+'), s.Flag('#'):
			io.WriteString(s, fn.Name())
			io.WriteString(s, "\n\t")
			io.WriteString(s, file)
		default:
			io.WriteString(s, fn.Name())
		}
		io.WriteString(s, ":")
		f.Format(s, 'd')
	case 'd':
		io.WriteString(s, strconv.Itoa(line))
	case 'n':
		io.WriteString(s, funcname(fn.Name()))
	}
}

func (f Caller) String() string {
	fn := runtime.FuncForPC(f.pc())
	if fn == nil {
		return "???:0"
	}
	file, line := fn.FileLine(f.pc())
	return fn.Name() + " " + filepath.Base(file) + ":" + strconv.Itoa(line)
}

type CallFrames []uintptr

func NewCallersDepth(calldepth int, depth int) CallFrames {
	if depth < 1 {
		depth = 1
	}
	pcs := make([]uintptr, depth)
	n := runtime.Callers(calldepth+1, pcs[:])
	var st CallFrames = pcs[0:n]
	return st
}

func NewCallers(depth int) CallFrames {
	return NewCallersDepth(2, depth)
}

func (c CallFrames) Format(st fmt.State, verb rune) {
	switch verb {
	case 's', 'v':
		switch {
		case st.Flag('+'):
			for _, pc := range c {
				caller := Caller(pc)
				fmt.Fprintf(st, "\n%+v", caller)
			}
		}
	}
}

// funcname removes the path prefix component of a function's name reported by func.Name().
func funcname(name string) string {
	i := strings.LastIndex(name, "/")
	name = name[i+1:]
	i = strings.Index(name, ".")
	return name[i+1:]
}
