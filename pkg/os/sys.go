package os

import (
	"os"
	"os/exec"
	"os/user"
	"strings"
)

func GetHostName() string {
	h, err := os.Hostname()
	if err == nil {
		return h
	}
	return ""
}

func GetUserName() string {
	current, err := user.Current()
	if err == nil {
		return current.Username
	}
	return ""
}

func ProcessExist(appName string) bool {
	cmd := exec.Command("ps", "-C", appName)
	output, _ := cmd.Output()
	fields := strings.Fields(string(output))
	for _, v := range fields {
		if v == appName {
			return true
		}
	}
	return false
}
