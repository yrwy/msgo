package errors

import (
	"fmt"
)

func Recover() error {
	if p := recover(); p != nil {
		//recover return a interface{}
		return fmt.Errorf("%v", p)
	}
	return nil
}
