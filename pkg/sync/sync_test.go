package sync

import (
	"fmt"
	"testing"
	"time"
)

func TestSync(t *testing.T) {
	w := NewSyncValue[int]()
	go func() {
		time.Sleep(time.Second)
		w.WaitSend(1)
		fmt.Println("wait send ok", time.Now())
	}()
	v := w.WaitRecv()
	fmt.Println("wait recv ok", time.Now(), v)
	if w.TimedSend(2, time.Second) {
		t.Fatal()
	}
	fmt.Println("timed send timeout", time.Now())
	if _, ok := w.TimedRecv(time.Second); ok {
		t.Fatal()
	}
	fmt.Println("timed recv timeout", time.Now())
	go func() {
		time.Sleep(time.Second)
		w.WaitSend(3)
		fmt.Println("wait send ok", time.Now())
	}()
	if v, ok := w.TimedRecv(time.Second * 3); ok {
		fmt.Println("timed recv ok", time.Now(), v)
	} else {
		t.Fatal()
	}
	go func() {
		if w.TimedSend(4, time.Second*3) {
			fmt.Println("timed send ok", time.Now())
		} else {
			panic("timed send error")
		}
	}()
	time.Sleep(time.Second)
	v = w.WaitRecv()
	fmt.Println("wait recv ok", time.Now(), v)
	w.Close()
}
