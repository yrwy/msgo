package sync

import (
	"context"
	"fmt"
	"os"
	"runtime"
	"sync/atomic"
	"testing"
	"time"
)

func TestPool(t *testing.T) {
	pool := NewGoPool()
	task := 0
	count := int32(0)
	fail := 0
	for i := 0; i < 100000; i++ {
		err := pool.Go(func(context.Context) {
			task++
			atomic.AddInt32(&count, 1)
		})
		if err != nil {
			fmt.Println(err)
		}
		ok := pool.TryGo(func(context.Context) {
			task--
			atomic.AddInt32(&count, -1)
		})
		if !ok {
			fail++
			runtime.Gosched()
		}
	}
	for pool.IsWorking() {
		time.Sleep(time.Second)
	}
	pool.Close()
	fmt.Println(task, count, fail)
}

func TestMustPool(t *testing.T) {
	pool := NewGoPool()
	task := make(chan int, 10)
	count := int32(0)
	fail := 0
	for i := 0; i < 1000; i++ {
		ii := i
		err := pool.MustGo(func(context.Context) {
			task <- ii
			atomic.AddInt32(&count, 1)
		})
		if err != nil {
			fmt.Println(err)
			break
		}
		err = pool.MustGo(func(context.Context) {
			i := <-task
			atomic.AddInt32(&count, -1)
			fmt.Println(os.Getpid(), i)
		})
		if err != nil {
			fmt.Println(err)
			break
		}
	}
	for pool.IsWorking() {
		time.Sleep(time.Second)
	}
	pool.Close()
	fmt.Println("pool.Close")
	close(task)
	fmt.Println(task, count, fail)
}
