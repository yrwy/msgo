package sync

import (
	"fmt"
	"testing"
	"time"
)

func TestWait(t *testing.T) {
	w := NewWaiter()
	go func() {
		time.Sleep(time.Second)
		w.WaitSend()
		fmt.Println("wait send ok", time.Now())
	}()
	w.WaitRecv()
	fmt.Println("wait recv ok", time.Now())
	if w.TimedSend(time.Second) {
		t.Fatal()
	}
	fmt.Println("timed send timeout", time.Now())
	if w.TimedRecv(time.Second) {
		t.Fatal()
	}
	fmt.Println("timed recv timeout", time.Now())
	go func() {
		time.Sleep(time.Second)
		w.WaitSend()
		fmt.Println("wait send ok", time.Now())
	}()
	if w.TimedRecv(time.Second * 3) {
		fmt.Println("timed recv ok", time.Now())
	} else {
		t.Fatal()
	}
	go func() {
		if w.TimedSend(time.Second * 3) {
			fmt.Println("timed send ok", time.Now())
		} else {
			panic("timed send error")
		}
	}()
	time.Sleep(time.Second)
	w.WaitRecv()
	fmt.Println("wait recv ok", time.Now(), v)
	w.Close()
}
