package sync

import (
	"sync"
)

func LockGuard(l sync.Locker, chunk func()) {
	l.Lock()
	defer l.Unlock()
	chunk()
}

type RLocker interface {
	RLock()
	RUnlock()
}

func RLockGuard(l RLocker, chunk func()) {
	l.RLock()
	defer l.RUnlock()
	chunk()
}
