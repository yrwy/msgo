package io

import (
	"crypto/md5"
	"fmt"
	"hash"
	"io"
	"math/rand"
	"os"
	"sync"
	"testing"
	"time"
)

func readStream(rs *RingStream, h hash.Hash) int {
	rand.Seed(time.Now().UnixNano())
	r := 0
	for {
		time.Sleep(time.Millisecond * time.Duration(50+rand.Intn(100)))
		n, e := rs.ReadBytes(0, func(p []byte) {
			h.Write(p)
		})
		r += n
		//fmt.Println("ReadBytes:", n, rs.Len())
		if e != nil {
			fmt.Println(n, e)
			break
		}
	}
	r += rs.TryReadBytes(0, func(p []byte) {
		if len(p) > 0 {
			fmt.Println("last read:", len(p))
			h.Write(p)
		}
	})
	fmt.Println("exit read")
	return r
}

func TestRingStream(t *testing.T) {
	rs := NewRingStream(0)
	n1 := 0
	md1 := md5.New()
	fp, er := os.Open("d:/tmp/test.data")
	if er != nil {
		fmt.Println(er)
		t.Fatal()
	}
	defer fp.Close()
	w := sync.WaitGroup{}
	w.Add(1)
	n2 := 0
	md2 := md5.New()
	go func() {
		defer w.Done()
		n2 = readStream(rs, md2)
	}()
	for {
		b := make([]byte, 4096)
		n, er := fp.Read(b)
		if er != nil {
			if er != io.EOF {
				fmt.Println(er)
				t.Fatal()
			}
			break
		}
		nr, er := rs.Write(b[:n])
		//fmt.Println("Write:", nr, rs.Len())
		n1 += n
		md1.Write(b[:n])
		//ws <- string(b[:n])
		if nr != n || er != nil {
			fmt.Println("write error:", er, nr, n)
			t.Fatal()
			break
		}
	}
	fmt.Println("RingStream close")
	rs.Close()
	fmt.Println("write over")
	w.Wait()
	s1 := md1.Sum(nil)
	s2 := md2.Sum(nil)
	fmt.Println(n1, n2)
	fmt.Println(s1)
	fmt.Println(s2)
	for i := 0; i < len(s1); i++ {
		if s1[i] != s2[i] {
			t.Fatal()
		}
	}
}

//go test -fuzz=Fuzz
func FuzzRingStream(f *testing.F) {
	rs := NewRingStream(0)
	n1 := 0
	md1 := md5.New()
	w := sync.WaitGroup{}
	w.Add(1)
	n2 := 0
	md2 := md5.New()
	go func() {
		defer w.Done()
		n2 = readStream(rs, md2)
	}()

	f.Add("abcdefg\n")
	f.Fuzz(func(t *testing.T, p string) {
		t.Log(p)
		n, e := rs.Write([]byte(p))
		if e != nil {
			t.Log(e)
			t.Fail()
		}
		n1 += n
		if n != len(p) {
			md1.Write([]byte(p[:n]))
			t.Log("fuzz write size error:", n, len(p))
			t.Fail()
		}
		md1.Write([]byte(p))
	})
	fmt.Println("RingStream close")
	rs.Close()
	fmt.Println("write over")
	w.Wait()
	s1 := md1.Sum(nil)
	s2 := md2.Sum(nil)
	fmt.Println(n1, n2)
	fmt.Println(s1)
	fmt.Println(s2)
	for i := 0; i < len(s1); i++ {
		if s1[i] != s2[i] {
			f.Fail()
		}
	}
}
