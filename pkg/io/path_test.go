package io

import (
	"os"
	"testing"
)

// go test -run=PathCanCreate
func TestPathCanCreate(t *testing.T) {
	name := "d:/temp/aaa.tar.gz"
	for i := 0; i < 10; i++ {
		filename := PathCanCreate(name)
		println(filename)
		f, err := os.OpenFile(filename, os.O_CREATE, os.ModePerm)
		if err != nil {
			println(err)
		}
		defer f.Close()
	}
	println("over")
}
